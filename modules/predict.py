import dill
import os
import json
import pandas as pd
from datetime import datetime

path = os.environ.get('PROJECT_PATH', '/opt/airflow_hw')


def predict():
    name = os.listdir(f'{path}/data/models/')
    with open(f'{path}/data/models/{name[0]}', 'rb') as file:
        model = dill.load(file)
    df_return = pd.DataFrame()
    for filename in os.listdir(f'{path}/data/test/'):
        with open(os.path.join(f'{path}/data/test/', filename)) as file_test:
            data = json.load(file_test)
            df = pd.DataFrame([data])
            x = {'car_id': df.id, 'predict': model.predict(df)}
            df_return = pd.concat([df_return, pd.DataFrame(x)], axis=0)

    df_return.to_csv(f'{path}/data/predictions/predict_{datetime.now().strftime("%Y%m%d%H%M")}.csv')


if __name__ == '__main__':
    predict()
